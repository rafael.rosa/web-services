package br.com.hackathon.util;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;

import com.sun.faces.util.MessageFactory;

public class FacesUtil {
	
	public static void addErrorMessage(String clientId, RNException message) {
		apresentarMensagem(clientId, FacesMessage.SEVERITY_ERROR, message.getMessage());
	}
	
	public static void addErrorMessage(String clientId, String message) {
		apresentarMensagem(clientId,FacesMessage.SEVERITY_ERROR, message);
	}
	
	public static void addErrorMessage(RNException message) {
		apresentarMensagem(FacesMessage.SEVERITY_ERROR, message.getMessage());
	}
	
	public static void addErrorMessage(String message) {
		apresentarMensagem(FacesMessage.SEVERITY_ERROR, message);
	}
	
	public static void addErrorMessageWithLabel(String message, FacesContext context, UIComponent component) {
		Object label = MessageFactory.getLabel(context, component);
		apresentarMensagem(FacesMessage.SEVERITY_ERROR, label + ": " + message); 
	}
	
	public static void addInfoMessage(RNException message) {
		apresentarMensagem(FacesMessage.SEVERITY_INFO, message.getMessage());
	}
	
	public static void addInfoMessage(String message) {
		apresentarMensagem(FacesMessage.SEVERITY_INFO, message);
	}
	
	public static void addInfoMessageWithLabel(String message, FacesContext context, UIComponent component) {
		Object label = MessageFactory.getLabel(context, component);
		apresentarMensagem(FacesMessage.SEVERITY_INFO, label + ": " + message); 
	}
	
	public static void addInfoMessage(String clientId, String message) {
		apresentarMensagem(clientId, FacesMessage.SEVERITY_INFO, message);
	}
	
	public static void addInfoMessage(String clientId, RNException message) {
		apresentarMensagem(clientId, FacesMessage.SEVERITY_INFO, message.getMessage());
	}
	
	public static void addWarnMessage(String clientId, RNException message) {
		apresentarMensagem(clientId, FacesMessage.SEVERITY_WARN, message.getMessage());
	}
	
	public static void addWarnMessage(String clientId, String message) {
		apresentarMensagem(clientId,FacesMessage.SEVERITY_WARN, message);
	}
	
	public static void addWarnMessage(RNException message) {
		apresentarMensagem(FacesMessage.SEVERITY_WARN, message.getMessage());
	}
	
	public static void addWarnMessage(String message) {
		apresentarMensagem(FacesMessage.SEVERITY_WARN, message);
	}
	
	public static void addWarnMessageWithLabel(String message, FacesContext context, UIComponent component) {
		Object label = MessageFactory.getLabel(context, component);
		apresentarMensagem(FacesMessage.SEVERITY_WARN, label + ": " + message); 
	}
	
	public static void addFatalMessage(String clientId, RNException message) {
		apresentarMensagem(clientId, FacesMessage.SEVERITY_FATAL, message.getMessage());
	}
	
	public static void addFatalMessage(String clientId, String message) {
		apresentarMensagem(clientId,FacesMessage.SEVERITY_FATAL, message);
	}
	
	public static void addFatalMessage(RNException message) {
		apresentarMensagem(FacesMessage.SEVERITY_FATAL, message.getMessage());
	}
	
	public static void addFatalMessage(String message) {
		apresentarMensagem(FacesMessage.SEVERITY_FATAL, message);
	}
	
	public static void addFatalMessageWithLabel(String message, FacesContext context, UIComponent component) {
		Object label = MessageFactory.getLabel(context, component);
		apresentarMensagem(FacesMessage.SEVERITY_FATAL, label + ": " + message); 
	}
	
	private static void apresentarMensagem(Severity severity, String message) {
		FacesContext.getCurrentInstance().addMessage(null, 
				new FacesMessage(severity, message, message));
	}
	
	private static void apresentarMensagem(String clientId, Severity severity, String message) {
		FacesContext.getCurrentInstance().addMessage(clientId, 
				new FacesMessage(severity, message, message));
		
		UIComponent comp = FacesContext.getCurrentInstance().
                getViewRoot().findComponent(clientId);
		if(comp instanceof UIInput) {
            ((UIInput) comp).setValid(false);
        }
	}
}