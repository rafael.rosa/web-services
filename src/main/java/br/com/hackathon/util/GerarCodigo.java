package br.com.hackathon.util;

import java.util.Random;

public class GerarCodigo {
	public static String gerarCodigoProvisoria(int tamanho) {
		String[] keys = new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", 
				"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o",
				"p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };
		
		StringBuilder senha = new StringBuilder();
		Random random = new Random();
		for(int i = 0; i < tamanho; i++) {
			int a = random.nextInt(keys.length);  
	        senha.append(keys[a]); 
		}
		return senha.toString();
	}
}
