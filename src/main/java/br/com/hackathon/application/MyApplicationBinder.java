package br.com.hackathon.application;

import org.glassfish.jersey.internal.inject.AbstractBinder;

import br.com.hackathon.negocio.UsuarioRN;

public class MyApplicationBinder extends AbstractBinder {
    @Override
    protected void configure() {
        bind(UsuarioRN.class).to(UsuarioRN.class);
    }
}