package br.com.hackathon.dao;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.hackathon.lib.entities.Usuario;
import br.com.hackathon.util.DAOException;

public class UsuarioDAO implements IDAO<Usuario>, Serializable {
	
	private static final long serialVersionUID = -4922064212221582612L;

	@Inject
	private Session session;

	@Override
	public Usuario saveOrUpdate(Usuario model) throws DAOException {
		try {
			model = (Usuario) session.merge(model);
			session.flush();
			session.clear();
			return model;
		} catch (Exception e) {
			throw new DAOException();
		}
	}

	@Override
	public void delete(Usuario model) throws DAOException {
		try {
			model = (Usuario) session.get(Usuario.class, model.getId());
			session.delete(model);
			session.flush();
			session.clear();
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@Override
	public Usuario findById(Usuario filtro) {
		return (Usuario) session.get(Usuario.class, filtro.getId());
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Usuario> findAll(Usuario filtros) {
		Criteria criteria = session.createCriteria(Usuario.class);
		criteria.addOrder(Order.asc(Usuario.Fields.EMAIL.toString()));
		return criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Usuario> findAtivos() {
		Criteria criteria = session.createCriteria(Usuario.class);
		criteria.add(Restrictions.eq(Usuario.Fields.ATIVO.toString(), true));
		criteria.addOrder(Order.asc(Usuario.Fields.EMAIL.toString()));
		return criteria.list();
	}

	public Usuario findByEmail(Usuario model) {
		Criteria criteria = session.createCriteria(Usuario.class);
		criteria.add(Restrictions.eq(Usuario.Fields.EMAIL.toString(), model.getEmail().toLowerCase()));
		criteria.add(Restrictions.ne(Usuario.Fields.ID.toString(), model.getId()));
		return (Usuario) criteria.uniqueResult();
	}

	public Usuario findByCodigo(String codigo) {
		String hql = "select u from " + Usuario.class.getSimpleName() + " u where "
					+ " u." + Usuario.Fields.CODIGO_HABILITAR_SENHA.toString() + " = :" + Usuario.Fields.CODIGO_HABILITAR_SENHA.toString();
		
		Query query = session.createQuery(hql);
		query.setString(Usuario.Fields.CODIGO_HABILITAR_SENHA.toString(), codigo.toLowerCase());
		
		return (Usuario) query.uniqueResult();
	}
}
