package br.com.hackathon.service;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.process.internal.RequestScoped;

import br.com.hackathon.lib.entities.Usuario;
import br.com.hackathon.negocio.UsuarioRN;
import br.com.hackathon.util.transactionHibernate.Transactional;

@Path("/execute-initial-data")
@RequestScoped
public class UsuarioService {

	@Inject
	private UsuarioRN usuarioRN;
	
	@GET
	@Transactional
	public Usuario get() {
		Usuario u = usuarioRN.findById(new Usuario(1));
		System.out.println("xxx");
		return u;
	}
	
}
