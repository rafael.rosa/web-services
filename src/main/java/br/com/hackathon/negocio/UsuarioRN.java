package br.com.hackathon.negocio;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.sun.xml.internal.messaging.saaj.packaging.mime.MessagingException;

import br.com.hackathon.dao.UsuarioDAO;
import br.com.hackathon.lib.entities.Usuario;
import br.com.hackathon.util.GerarCodigo;
import br.com.hackathon.util.MailException;
import br.com.hackathon.util.MensagemUtil;
import br.com.hackathon.util.RNException;

public class UsuarioRN implements IRN<Usuario>, Serializable {

	private static final long serialVersionUID = 9213921528571040767L;
	
	@Inject
	private UsuarioDAO usuarioDAO;
	
	@Override
	public Usuario saveOrUpdate(Usuario model) throws RNException {
		//Verifica se o e-mail j� existe
		/*if(usuarioDAO.findByEmail(model) != null) {
			throw new RNException(MensagemUtil.getMensagem("erro_email_existente"));
		}
		
		try {
			model = usuarioDAO.saveOrUpdate(model);
		} catch (Exception exception) {
			exception.printStackTrace();
			throw new RNException(MensagemUtil.getMensagem("erro_salvar"));
		}
		return model;*/
		return null;
	}

	@Override
	public void delete(Usuario model) throws RNException {
		/*try {
			if(model.isCodigoHabilitarSenhaAtivo() == false)
				throw new RNException(MensagemUtil.getMensagem("erro_excluir_usuario_ativo"));
			usuarioDAO.delete(model);
		} catch (DAOException e) {
			throw new RNException(MensagemUtil.getMensagem("erro_excluir") + " " + e.getMessage());
		}*/
		
	}

	@Override
	public Usuario findById(Usuario filtro) {
		return usuarioDAO.findById(filtro);
	}

	@Override
	public List<Usuario> findAll() {
		return null;//usuarioDAO.findAll(null);
	}
	
	public List<Usuario> findAtivos() {
		return null; //usuarioDAO.findAtivos();
	}
	
	/*public Usuario findByEmail(Usuario model) {
		return usuarioDAO.findByEmail(model);
	}
	
	public Usuario findAtivoByEmail(String email) {
		Usuario filtro = new Usuario();
		filtro.setEmail(email);
		Usuario usuario = usuarioDAO.findByEmail(filtro);
		if(usuario != null && usuario.isAtivo() == true)
			return usuario;
		else
			return null;
	}
	
	public Usuario verificarCodigoAtivoAtivarUsuario(String codigo) {
		if(codigo == null || codigo.isEmpty()) {
			return null;
		}
		Usuario usuario = usuarioDAO.findByCodigo(codigo);
		if(usuario == null || !usuario.isCodigoHabilitarSenhaAtivo()) { //verifica se existe o c�digo e se est� habilitado a criar nova senha
			return null;
		} else {
			return usuario;
		}
	}
	
	public Usuario verificarCodigoAtivoRecuperarSenha(String codigo) {
		if(codigo == null || codigo.isEmpty()) {
			return null;
		}
		Usuario usuario = usuarioDAO.findByCodigo(codigo);
		//Verifica se j� tem alguma senha registrada e se tem c�digo
		if(usuario != null && usuario.isCodigoHabilitarSenhaAtivo() && usuario.getCodigoHabilitarSenha().equals(codigo) && usuario.getSenha() != null) {
			return usuario;
		} else {
			return null;
		}
	}

	public void salvarAtivarUsuario(Usuario usuario) throws RNException {
		usuario.setCodigoHabilitarSenha(null);
		usuario.setCodigoHabilitarSenhaAtivo(false);
		usuario.setDtaHraRegistro(new Date());
		usuario.setAtivo(true);
		usuario.setSenha(GeradorSenhaHash.gerarHash(usuario.getSenha()));
		
		try {
			usuarioDAO.saveOrUpdate(usuario);
		} catch (DAOException e) {
			e.printStackTrace();
			throw new RNException(MensagemUtil.getMensagem("erro_salvar"));
		}
	}
	
	public void salvarRecuperarSenha(Usuario usuario) throws RNException {
		usuario.setCodigoHabilitarSenha(null);
		usuario.setCodigoHabilitarSenhaAtivo(false);
		usuario.setSenha(GeradorSenhaHash.gerarHash(usuario.getSenha()));
		
		try {
			usuarioDAO.saveOrUpdate(usuario);
		} catch (DAOException e) {
			e.printStackTrace();
			throw new RNException(MensagemUtil.getMensagem("erro_salvar"));
		}
	}*/

	public void saveOrUpdate(Usuario usuario, String senhaAtual) throws RNException {
		Usuario usuarioNaBase = findById(usuario);
		
		/*if(GeradorSenhaHash.isPasswordValid(senhaAtual, usuarioNaBase.getSenha())) {
			usuario.setSenha(
					GeradorSenhaHash.gerarHash(usuario.getSenha()));
			saveOrUpdate(usuario);
		} else {
			throw new RNException(MensagemUtil.getMensagem("erro_senha_invalida"));
		}*/
	}
	
	public void reenviarEmailParaRecuperarSenha(String email) throws MessagingException, MailException, RNException {
		Client client = ClientBuilder.newClient();
        
        WebTarget webTarget = client.target("http://localhost:8080/projetorest/byemail");
        
        Invocation.Builder invocationBuilder = webTarget.request();
         
        Response response = invocationBuilder.get();
         
        if(Status.OK.getStatusCode() == response.getStatus()) {
        	Usuario usuario = (Usuario) response.getEntity();
    		usuario.setCodigoHabilitarSenha(GerarCodigo.gerarCodigoProvisoria(50));
    		usuario.setCodigoHabilitarSenhaAtivo(true);

    		//Envia o e-mail
    		/*EmailService emailService = new EmailService();
    		emailService.sendEmailByAdministrador(TipoEmails.RECUPERAR_SENHA, usuario);*/
        } else {
			throw new RNException(MensagemUtil.getMensagem("erro_esqueci_minha_senha_email_nao_cadastrado"));
        }
	}
}